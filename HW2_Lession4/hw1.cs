﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HW2_Lession4
{
    internal class hw1
    {
        public void TriangleCheck()
        {
            double edge1, edge2, edge3;

            Console.Write("EDGE 1 = ");
            edge1 = double.Parse(Console.ReadLine());
            Console.Write("EDGE 2 = ");
            edge2 = double.Parse(Console.ReadLine());
            Console.Write("EDGE 3 = ");
            edge3 = double.Parse(Console.ReadLine());


            if((edge1 >= edge2 + edge3) || (edge2 >= edge1 + edge3) || (edge3 >= edge1 + edge2))
            {
                Console.WriteLine("Not is triangle!");
            }

            else if((edge1 < edge2 + edge3) && (edge2 < edge1 + edge3) && (edge3 < edge1 + edge2))
            {
                Console.Write("This is : ");
                if(edge1 == edge2 && edge2 == edge3) 
                {
                    Console.WriteLine("equilateral triangle");
                }
                else if(edge1 == edge2 || edge1 == edge3 || edge2 == edge3)
                {
                    Console.WriteLine("isosceles triangle");
                }
                else if((edge1*edge1 + edge2*edge2 == edge3*edge3) || 
                        (edge1 * edge1 + edge3 * edge3 == edge1 * edge1) || 
                        (edge2 * edge2 + edge3 * edge3 == edge1 * edge1))
                {
                    Console.WriteLine("right triangle");
                }

            }
        }
    }
}
